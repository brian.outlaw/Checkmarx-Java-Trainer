FROM openjdk:8-alpine
COPY build/libs/checkmarx-trainer-java-1.0.0.jar checkmarx-java-trainer.jar
CMD java -jar checkmarx-java-trainer.jar
EXPOSE 4567